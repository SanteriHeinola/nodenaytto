function oppilashaku() {
    var xmlhttp = new XMLHttpRequest();

    function reqListener() {

        var myObj = JSON.parse(this.responseText);
        console.log(myObj);
        var txt = "<table border='1'>"
        console.log(myObj)
        txt +=
            `<thead>
                        <td>Student Number</td>
                        <td>First Name</td>
                        <td>Last name</td>
                        <td>Email</td>
                        <td>Gender</td></thead><tbody>`;

        myObj.forEach(function (student) {
            console.log(student);
            txt +=
                `<tr>
                            <td>${student.student_number}
                            <td>${student.first_name}
                            <td>${student.last_name}
                            <td>${student.email}
                            <td>${student.gender}`;
        });
        txt += "</tbody></table>"
        document.getElementById("studenthaku").innerHTML = txt;
    };
    var oReq = new XMLHttpRequest();
    oReq.addEventListener("load", reqListener);
    oReq.open("GET", "http://127.0.0.1:8081/listStudents", true);
    oReq.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    oReq.send("");
}

function lisaaOppilas(e) {
    console.log(e);
    e.preventDefault();

    var student_number = e.target.elements.student_number.value;
    var first_name = e.target.elements.first_name.value;
    var last_name = e.target.elements.last_name.value;
    var email = e.target.elements.email.value;
    var gender = e.target.elements.gender.value;

    var data = {
        student_number: student_number,
        first_name: first_name,
        last_name: last_name,
        email: email,
        gender: gender
    };

    console.log(data);

    var xmlhttp = new XMLHttpRequest();

    function reqListener(e) {
        console.log(e)

    };
    var oReq = new XMLHttpRequest();
    oReq.addEventListener("load", reqListener);
    oReq.open("POST", "http://127.0.0.1:8081/addStudent", true);
    oReq.setRequestHeader("Content-type", "application/json");
    oReq.send(JSON.stringify(data));
}

document.getElementById("lisaa-oppilas").addEventListener("submit", lisaaOppilas);

function haeOppilas(e) {
    console.log(e)
    e.preventDefault();

    var id = e.target.elements._id.value
    var data = {
        id: id
    };
    console.log(data);

    var xmlhttp = new XMLHttpRequest();

    function reqListener(e) {
        console.log(e)

    };
    
    var oReq = new XMLHttpRequest();
    oReq.addEventListener("load", reqListener);
    oReq.open("DELETE", "http://127.0.0.1:8081/deleteStudent", true);
    oReq.setRequestHeader("Content-type", "application/json");
    oReq.send(JSON.stringify(data));
    
}
document.getElementById("hae-oppilas-form").addEventListener("submit", haeOppilas);

function poistaOppilas(e) {
    console.log(e)
    e.preventDefault();

    var id = e.target.elements._id.value
    var data = {
        id: id
    };
    console.log(data);

    var xmlhttp = new XMLHttpRequest();

    function reqListener(e) {
        console.log(e)

    };

    var oReq = new XMLHttpRequest();
    oReq.addEventListener("load", reqListener);
    oReq.open("DELETE", "http://127.0.0.1:8081/:id", true);
    oReq.setRequestHeader("Content-type", "application/json");
    oReq.send(JSON.stringify(data));

}
document.getElementById("poista-oppilas-form").addEventListener("submit", poistaOppilas);