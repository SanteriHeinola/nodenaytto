var express = require('express');
var app = express();
var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var url = "mongodb://developer:devpass1@ds123224.mlab.com:23224/uni";
var bodyParser = require('body-parser');

app.use(bodyParser.urlencoded());
app.use(bodyParser.json());

app.get('/:id', function (req, res) {
  MongoClient.connect(url, function (err, db) {
    if (err) {
      console.log(err);
      res.status(400).send(err);
    }


    var dbo = db.db("uni");
    dbo.collection("students").find({
      "_id": ObjectId("5bfeac87079ce32b74d6fa35")
    }).toArray(function (err, result) {
      if (err) {
        console.log(err);
        res.status(400).send(err);
      } else if (result.length < 1) {
        res.status(204).send("No such student found");
      } else {
        console.log(result);
        res.status(200).send(result);
      }
      db.close();
    });
  });
})

var http = require('http');
var server = app.listen(8081, function () {
  var host = server.address().address
  var port = server.address().port
  console.log("Kuunnellaan http://%s:%s", host, port)

})