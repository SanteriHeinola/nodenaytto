var express = require('express');
var app = express();
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://developer:devpass1@ds123224.mlab.com:23224/uni";

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get('/listStudents', function (req, res) {
  MongoClient.connect(url, function (err, db) {
    if (err) {
      console.log(err);
      res.status(400).send(err);
    }
    var dbo = db.db("uni");
    dbo.collection("students").find({}).toArray(function (err, result) {
      if (err) {
        console.log(err);
        res.status(400).send(err);
      } else if (result.length < 1) {
        res.status(204).send("No students in database");
      } else {
        console.log(result);
        res.status(200).send(result);
      }
      db.close();
    });
  });
})

var server = app.listen(8081, function () {
  var host = server.address().address
  var port = server.address().port

  console.log("Kuunnellaan http://%s:%s", host, port)
})