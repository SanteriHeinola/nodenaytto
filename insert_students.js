var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://developer:devpass1@ds123224.mlab.com:23224/uni";
//mongodb: //<dbuser>:<dbpassword>@ds123224.mlab.com:23224/webkehitysmooc
MongoClient.connect(url, function (err, db) {
  if (err) {
    console.log(err);
    res.status(400).send(err);
  }
  var dbo = db.db("uni");
  var myobj = [{
    "student_number": 1,
    "first_name": "Jeanette",
    "last_name": "Penddreth",
    "email": "jpenddreth0@census.gov",
    "gender": "Female"
  }, {
    "student_number": 2,
    "first_name": "Noell",
    "last_name": "Frediani",
    "email": "gfrediani1@senate.gov",
    "gender": "Male"
  }, {
    "student_number": 3,
    "first_name": "Noell",
    "last_name": "Bea",
    "email": "nbea2@imageshack.us",
    "gender": "Female"
  }, {
    "student_number": 4,
    "first_name": "Willard",
    "last_name": "Valek",
    "email": "wvalek3@vk.com",
    "gender": "Male"
  }];
  dbo.collection("students").insertMany(myobj, function (err, res) {
    if (err) {
      console.log(err);
      res.status(204).send("Could not insert");

    } else {
      console.log("Number of documents inserted: " + res.insertedCount);
    }
    db.close();
  });
})