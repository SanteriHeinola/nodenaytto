var express = require('express');
var app = express();
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://developer:devpass1@ds123224.mlab.com:23224/uni";
var bodyParser = require('body-parser');
var ObjectId = require('mongodb').ObjectID;


app.use(bodyParser.urlencoded());
app.use(bodyParser.json());

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.get('/listStudents', function (req, res) {
    MongoClient.connect(url, function (err, db) {
        if (err) {
            console.log(err);
            res.status(400).send(err);
        }
        var dbo = db.db("uni");
        dbo.collection("students").find({}).toArray(function (err, result) {
            if (err) {
                console.log(err);
                res.status(400).send(err);
            } else if (result.length < 1) {
                res.status(204).send("No students in database");
            } else {
                console.log(result);
                res.status(200).send(result);
            }
            db.close();
        });
    });
})

app.post('/addStudent', function (req, res) {
    console.log(req.body)
    MongoClient.connect(url, function (err, db) {
        if (err) {
            console.log(err);
            res.status(400).send(err);
        }
        var dbo = db.db("uni");
        var myobj = [{
            student_number: req.body.student_number,
            first_name: req.body.first_name,
            last_name: req.body.last_name,
            email: req.body.email,
            gender: req.body.gender
        }];

        dbo.collection("students").insertMany(myobj, function (err, response) {
            if (err) {
                console.log(err);
                res.status(204).send("Could not insert");
            } else {
                console.log("New student inserted");
                console.log("Number of documents inserted: " + res.insertedCount);
                res.status(200).send(response);
            }
            db.close();
        });
    })
})

app.delete('/deleteStudent', function (req, res) {
    console.log(req.body)
    MongoClient.connect(url, function (err, db) {
        if (err) {
            console.log(err);
            res.status(400).send(err);
        }

        var dbo = db.db("uni");
        dbo.collection("students").deleteOne({
            "_id": ObjectId(req.body.id)
        }, function (err, response) {
            if (err) {
                console.log(err);
                res.status(400).send(err);
            } else {
                //console.log(response);
                console.log(response.deletedCount + " document deleted");
                res.status(200).send(response);
            }
            db.close();
        });
    })
});

app.get('/:id', function (req, res) {
    MongoClient.connect(url, function (err, db) {
        if (err) {
            console.log(err);
            res.status(400).send(err);
        }

        var dbo = db.db("uni");
        dbo.collection("students").find({
            "_id": ObjectId("5bfeac87079ce32b74d6fa35")
        }).toArray(function (err, result) {
            if (err) {
                console.log(err);
                res.status(400).send(err);
            } else if (result.length < 1) {
                res.status(204).send("No such student found");
            } else {
                console.log(result);
                res.status(200).send(result);
            }
            db.close();
        });
    });
})

var server = app.listen(8081, function () {
    var host = server.address().address
    var port = server.address().port

    console.log("Kuunnellaan http://%s:%s", host, port)
})