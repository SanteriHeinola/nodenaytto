const MongoClient = require('mongodb').MongoClient;
const url = "mongodb://developer:devpass1@ds123224.mlab.com:23224/uni";
var express = require('express');
var app = express();
var bodyParser = require('body-parser');

app.use(bodyParser.urlencoded());
app.use(bodyParser.json());


app.use(express.static('public'));

app.get('/post_student.htm', function (req, res) {
   res.sendFile(__dirname + "/" + "post_student.htm");
})

app.post('/addStudent', function (req, res) {
   console.log(req.body)
   MongoClient.connect(url, function (err, db) {
      if (err) {
         console.log(err);
         res.status(400).send(err);
      }
      var dbo = db.db("uni");
      var myobj = [{
         student_number: req.body.student_number,
         first_name: req.body.first_name,
         last_name: req.body.last_name,
         email: req.body.email,
         gender: req.body.gender
      }];

      dbo.collection("students").insertMany(myobj, function (err, response) {
         if (err) {
            console.log(err);
            res.status(204).send("Could not insert");
         } else {
            console.log("New student inserted");
            console.log("Number of documents inserted: " + res.insertedCount);
            res.status(200).send(result);
         }
         db.close();
      });
   })
})

var server = app.listen(8081, function () {
   var host = server.address().address
   var port = server.address().port

   console.log("Kuunnellaan http://%s:%s", host, port)
})